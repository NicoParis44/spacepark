package org.miage.spacepark.mock;

import android.widget.ArrayAdapter;

import org.miage.spacepark.model.NomParking;
import org.miage.spacepark.model.Parking;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by utilisateur on 17/02/2018.
 */

public class ParkingMocker {


    public static List<Parking> listeParking() {
        ArrayList<Parking> parkingArrayList = new ArrayList<>();
        for(int i = 1; i < 11; i++) {
            Parking parking = new Parking();
            parking.setNomAdresse("Adresse " + String.valueOf(i));
            parking.setNomParking("Nom " + String.valueOf(i));
            parking.setNbPlaceVoiture(i);

            parkingArrayList.add(parking);
        }



        return parkingArrayList;
    }

}
