package org.miage.spacepark.event;

import org.miage.spacepark.model.ParkingReponse;

/**
 * Created by utilisateur on 19/02/2018.
 */

public class SearchResultEvent {

    private ParkingReponse listeParking;

    public SearchResultEvent(ParkingReponse lParking) {
        this.listeParking = lParking;
    }

    public ParkingReponse getParking() {
        return listeParking;
    }

}
