package org.miage.spacepark;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.miage.spacepark.event.EventBusManager;
import org.miage.spacepark.event.SearchResultEvent;
import org.miage.spacepark.model.DisponibiliteParking;
import org.miage.spacepark.model.DisponibiliteParkingReponse;
import org.miage.spacepark.model.Parking;
import org.miage.spacepark.model.ParkingReponse;
import org.miage.spacepark.model.dao.DaoDisponibilite;
import org.miage.spacepark.model.dao.DaoParking;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static com.activeandroid.Cache.getContext;


/**
 * Cette classe permet d'appeller les API Rest afin de récupéré les différentes données sur les parkings
 */
public class ParkingSearchService {
    private static final long REFRESH_DELAY = 300000;
    public static ParkingSearchService INSTANCE = new ParkingSearchService();
    private final ParkingSearchRESTService mParkingSearchRESTService;
    private ScheduledExecutorService mScheduler = Executors.newScheduledThreadPool(1);
    private ScheduledFuture mLastScheduleTask;

    private String url = "https://data.nantes.fr";

    private ParkingSearchService() {
        // Create GSON Converter that will be used to convert from JSON to Java
        Gson gsonConverter = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .serializeNulls()
                .excludeFieldsWithoutExposeAnnotation().create();

        // Create Retrofit client
        Retrofit retrofit = new Retrofit.Builder()
                // Using OkHttp as HTTP Client
                .client(new OkHttpClient())
                // Having the following as server URL
                .baseUrl(url)
                // Using GSON to convert from Json to Java
                .addConverterFactory(GsonConverterFactory.create(gsonConverter))
                .build();

        // Use retrofit to generate our REST service code
        mParkingSearchRESTService = retrofit.create(ParkingSearchRESTService.class);
    }

    public void searchParking() {

        if(isDeviceOnline() || !DaoParking.allParking().isEmpty()) {

            if(isDeviceOnline()) {
                if (mLastScheduleTask != null && !mLastScheduleTask.isDone()) {
                    ParkingReponse listeParking = completerReponseHorsDelai();
                    EventBusManager.BUS.post(new SearchResultEvent(listeParking));
                    mLastScheduleTask.cancel(true);
                }
                // Cancel last scheduled network call (if any)
                // Schedule a network call in REFRESH_DELAY ms
                /* Quand les deux lignes du dessus sont décommenter, on ne récupère plus les données des API,
                ainsi on ne met plus a jour, ou on enregistre plus les données dans la base de données.
                 */
/*                mLastScheduleTask = mScheduler.schedule(new Runnable() {
                    public void run() {*/
                        mParkingSearchRESTService.searchForDisponibilite("json").enqueue(new Callback<DisponibiliteParkingReponse>() {
                            @Override
                            public void onResponse(Call<DisponibiliteParkingReponse> call, Response<DisponibiliteParkingReponse> response) {
                                System.out.println(response);
                                if (response != null && response.body() != null) {
                                    DisponibiliteParkingReponse reponseApi = response.body();
                                    List<DisponibiliteParking> listeParking = reponseApi.getContenuReponse().getReponse().getDonnees()
                                            .getGroupesParking().getDisponibiliteParkings();
                                    DaoDisponibilite.enregistrerDisponilite(listeParking);


                                }
                            }

                            @Override
                            public void onFailure(Call<DisponibiliteParkingReponse> call, Throwable t) {
                                System.out.println("Fail");
                            }
                        });

                        List<Parking> reponseBDD = DaoParking.allParking();
                        if (reponseBDD != null) {
                            ParkingReponse res = new ParkingReponse();
                            res.setListeParking(reponseBDD);
                            EventBusManager.BUS.post(new SearchResultEvent(res));
                        }

                        //System.out.println(reponseBDD.size());


                        // Call to the REST service
                        mParkingSearchRESTService.searchForParking("json").enqueue(new Callback<ParkingReponse>() {
                            @Override
                            public void onResponse(Call<ParkingReponse> call, retrofit2.Response<ParkingReponse> response) {
                                // Post an event so that listening activities can update their UI
                                System.out.println(response);
                                if (response != null && response.body() != null) {
                                    ParkingReponse listeParking = completerReponse(response.body());
                                    EventBusManager.BUS.post(new SearchResultEvent(listeParking));
                                }
                            }

                            @Override
                            public void onFailure(Call<ParkingReponse> call, Throwable t) {
                                // Request has failed or is not at expected format
                                // We may want to display a warning to user (e.g. Toast)
                                System.out.println("Fail");
                            }
                        });


/*                    }
                }, REFRESH_DELAY, TimeUnit.MILLISECONDS);*/

            } else {
                ParkingReponse listeParking = completerReponseHorsDelai();
                EventBusManager.BUS.post(new SearchResultEvent(listeParking));
                Toast.makeText(getContext(), "Les données sont peut être erronés", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getContext(), "Internet est nécessaire pour charger une 1ere fois les parkings", Toast.LENGTH_SHORT).show();

        }
    }

    /**
     * Interface permettant l'appel des différentes APIs
     */
    public interface ParkingSearchRESTService {

        @GET("/api/publication/24440040400129_NM_NM_00044/LISTE_SERVICES_PKGS_PUB_NM_STBL/content/")
        Call<ParkingReponse> searchForParking(@Query("format") String format);

        @GET("/api/getDisponibiliteParkingsPublics/1.0/724KH1XWJU27J4B//")
        Call<DisponibiliteParkingReponse> searchForDisponibilite(@Query("output") String format);


    }

    /**
     * Méthode pour remplir la réponse avec les données reçues de l'API
     * @param reponse la reponse de l'api
     * @return
     */
    private ParkingReponse completerReponse(ParkingReponse reponse) {
        ParkingReponse newReponse = new ParkingReponse();
        newReponse.setNbResultat(reponse.getNbResultat());
        newReponse.setVersion(reponse.getVersion());
        ArrayList<Parking> listeParking = new ArrayList<Parking>();
        newReponse.setListeParking(listeParking);
        for(Parking parking : reponse.getListeParking()) {

            parking.setNomParking(parking.getpNomParking().getNom());
            parking.setLatitude(parking.getGeolocalisation().get(0));
            parking.setLongitude(parking.getGeolocalisation().get(1));
            parking.setCB(parking.getMoyenPaiement()!= null && parking.getMoyenPaiement().contains("CB"));
            parking.setCheque(parking.getMoyenPaiement()!= null && parking.getMoyenPaiement().contains("chèque"));
            parking.setEspece(parking.getMoyenPaiement()!= null && parking.getMoyenPaiement().contains("Espèces"));
            DisponibiliteParking dispo = DaoDisponibilite.getDisponibiliteById(parking.getIdParking());
            if(dispo != null)
                parking.setNbPlacesDisponible(Integer.valueOf(dispo.getNbPlacesDisponible()));
            listeParking.add(parking);

            parking.save();
        }
        return newReponse;
    }

    /**
     * Récupére les données de la passe de donnée quand 5 mn ne sont pas écoulé depuis le
     * dernier appel de la classe
     * @return
     */
    private ParkingReponse completerReponseHorsDelai() {
        ParkingReponse newReponse = new ParkingReponse();
        ArrayList<Parking> listeParking = new ArrayList<Parking>();
        List<Parking>  reponse = DaoParking.allParking();
        newReponse.setListeParking(reponse);
        return newReponse;
    }

    /**
     * Verifie si il y a de la connexion internet
     * @return
     */
    public static boolean isDeviceOnline() {
        ConnectivityManager connMgr = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

}