package org.miage.spacepark.vue;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;


import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.SupportStreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;

import org.miage.spacepark.MainActivity;
import org.miage.spacepark.ParkingSearchService;
import org.miage.spacepark.R;
import org.miage.spacepark.model.Favori;
import org.miage.spacepark.model.Parking;
import org.miage.spacepark.model.dao.DaoFavori;
import org.miage.spacepark.model.dao.DaoParking;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by utilisateur on 12/04/2018.
 */

public class ActivityDetailParking extends Menu {

    @BindView(R.id.nomParking)
    TextView nom;

    @BindView(R.id.presentation)
    TextView presentation;

    @BindView(R.id.adresse)
    TextView adresse;

    @BindView(R.id.placeDisponible)
    TextView placeDisponible;

    @BindView(R.id.capaciteVoiture)
    TextView capaciteVoiture;

    @BindView(R.id.capaciteAutoElectrique)
    TextView capaciteAutoElect;

    @BindView(R.id.capaciteMoto)
    TextView capaciteMoto;

    @BindView(R.id.capacitePMR)
    TextView capacitePMR;

    @BindView(R.id.capaciteVelo)
    TextView capaciteVelo;

    @BindView(R.id.transports)
    TextView transports;

    @BindView(R.id.transportsCommuns)
    TextView transportsCommuns;

    @BindView(R.id.telephone)
    TextView telephone;

    @BindView(R.id.webSite)
    TextView webSite;

    @BindView(R.id.btnFavori)
    Button btnFavori;

    @BindView(R.id.paiements)
    TextView paiements;

    @BindView(R.id.cb)
    ImageView cb;

    @BindView(R.id.cheque)
    ImageView cheque;

    @BindView(R.id.espece)
    ImageView espece;

    @BindView(R.id.contact)
    TextView contact;

    @BindView(R.id.telephoneIcone)
    ImageView telephoneIcone;

    @BindView(R.id.siteWebIcone)
    ImageView siteWebIcone;

    @BindView(R.id.transportsIcone)
    ImageView transportsIcone;

    @BindView(R.id.paiementsLineUp)
    View paiementsLineUp;

    @BindView(R.id.paiementsLineDown)
    View paiementsLineDown;

    @BindView(R.id.transportsLineUp)
    View transportsLineUp;

    @BindView(R.id.transportsLineDown)
    View transportsLineDown;

    @BindView(R.id.contactLineUp)
    View contactLineUp;

    @BindView(R.id.contactLineDown)
    View contactLineDown;

    @BindView(R.id.MapsViewButton)
    ImageView MapsViewButton;

    private Parking parking;

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_detail_parking);
        setContentView(R.layout.activity_detail_parking);
        ButterKnife.bind(this);
        initialiserMenu();
        loadDetailParking();
        loadCapaciteParking();
        loadMoyenPaiements();
        loadContacts();
        if (isFavori()) {
            btnFavori.setText("- Retirer de mes favoris");
        } else {
            btnFavori.setText("+ Ajouter à mes favoris");
        }

        SupportStreetViewPanoramaFragment streetViewPanoramaFragment = new SupportStreetViewPanoramaFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.streetviewpanorama, streetViewPanoramaFragment)
                .commit();

        if(!ParkingSearchService.isDeviceOnline()){
            FrameLayout frameLayout = (FrameLayout) findViewById(R.id.streetviewpanorama);
            frameLayout.setVisibility(View.GONE);
        }
        streetViewPanoramaFragment.getStreetViewPanoramaAsync(
                new OnStreetViewPanoramaReadyCallback() {
                    @Override
                    public void onStreetViewPanoramaReady(StreetViewPanorama panorama) {
                        // Only set the panorama to SYDNEY on startup (when no panoramas have been
                        // loaded which is when the savedInstanceState is null).
                        if (savedInstanceState == null) {
                            LatLng positionParking = new LatLng(parking.getLatitude(), parking.getLongitude());
                            panorama.setPosition(positionParking);
                        }
                    }
                });

    }

    /**
     * Paramétrage de l'affichage selon le nombre de place pour les différents
     * type de véhicules
     */
    private void loadCapaciteParking() {
        placeDisponible.setText("Places restantes : "+parking.getNbPlacesDisponible() + "/" + parking.getNbPlaceVoiture());
        capaciteVoiture.setText(String.valueOf(parking.getNbPlaceVoiture()));
        capaciteAutoElect.setText(String.valueOf(parking.getNbPlaceVoitureElectrique()));
        capacitePMR.setText(String.valueOf(parking.getNbPlaceHandicape()));
        capaciteMoto.setText(String.valueOf(parking.getNbPlaceMoto()));
        capaciteVelo.setText(String.valueOf(parking.getNbPlaceVelo()));
        String transport = parking.getTransportCommun();
        if(transport != null) {
            String[] acces = transport.split("\\.");
            transport="";
            for (String acce : acces) {
                transport = transport + acce + "\n";
            }
            transportsCommuns.setText(transport);
        } else {
            transportsCommuns.setText("");
            transports.setVisibility(View.GONE);//on masque le titre
            transportsLineDown.setVisibility(View.GONE);//on masque la barre
            transportsLineUp.setVisibility(View.GONE);//on masque la barre
            transportsIcone.setVisibility(View.GONE);//on masque l'icone
        }

    }

    /**
     * Paramétrage de l'affichage des moyens de paiement
     */
    private void loadMoyenPaiements(){
        if(!parking.isCB() && !parking.isCheque() && !parking.isEspece()){
            cb.setVisibility(View.GONE);//on masque l'icone
            espece.setVisibility(View.GONE);//on masque l'icone
            cheque.setVisibility(View.GONE);//on masque l'icone
            paiements.setVisibility(View.GONE);//on masque le titre
            paiementsLineUp.setVisibility(View.GONE);//on masque la barre
            paiementsLineDown.setVisibility(View.GONE);//on masque la barre
        } else {
            if(!parking.isCB()){//S'il n'y a pas de paiement CB
                cb.setVisibility(View.GONE);//on masque l'icone
            }
            if(!parking.isEspece()){//S'il n'y a pas de paiement Espèce
                espece.setVisibility(View.GONE);//on masque l'icone
            }
            if(parking.isCheque()){//S'il n'y a pas de paiement cheque
                cheque.setVisibility(View.GONE);//on masque l'icone
            }
        }
    }

    /**
     * Parametrage de l'affichage des contacts
     */
    private void loadContacts(){
        telephone.setText(parking.getNumeroTelephone());
        webSite.setText(parking.getSiteWeb());
        if(parking.getNumeroTelephone()==null && parking.getSiteWeb()==null){
            telephoneIcone.setVisibility(View.GONE);//on masque l'icone
            siteWebIcone.setVisibility(View.GONE);//on masque l'icone
            contact.setVisibility(View.GONE);//on masque le titre
            contactLineUp.setVisibility(View.GONE);//on masque la barre
            contactLineDown.setVisibility(View.GONE);//on masque la barre
        } else {
            if(parking.getNumeroTelephone()==null){
                telephoneIcone.setVisibility(View.GONE);//on masque l'icone
            }
            if(parking.getSiteWeb()==null){
                siteWebIcone.setVisibility(View.GONE);//on masque l'icone
            }
        }

    }

    /**
     * Charge le parking selon son id
     */
    private void loadDetailParking() {

        int idParking = getIntent().getIntExtra(MainActivity.PARKING_SELECTIONNER, 0);
        parking = DaoParking.getParkingByIdParking(idParking);
        loadDescriptionParking();

    }

    /**
     * Paramétrage de l'affichage de la description du parking
     */
    private void loadDescriptionParking() {
        nom.setText(parking.getNomParking());
        if(parking.getPresentation() != null) {
            presentation.setText(parking.getPresentation());
        } else {
            presentation.setVisibility(View.GONE);//on masque le texte
        }
        adresse.setText(constituerAdresse());
    }

    /**
     * Création de l'adresse du parking avec les données
     * @return
     */
    private String constituerAdresse() {

        StringBuilder adresseConc = new StringBuilder();
        adresseConc.append(parking.getNomAdresse());
        adresseConc.append(" ");
        adresseConc.append(String.valueOf(parking.getCodePostale()));
        adresseConc.append(" ");
        adresseConc.append(parking.getCommune());

        return adresseConc.toString();
    }

    /**
     * Traitement de la mise ou non en favori
     */
    @OnClick(R.id.btnFavori)
    public void taitementFavori() {
        if(isFavori()){
            Favori favori = new Favori();
            favori.setIdParking(parking.getIdParking());
            favori.setFavori(false);
            favori.save();
            btnFavori.setText("+ Ajouter à mes favoris");
        } else {
            Favori favori = new Favori();
            favori.setIdParking(parking.getIdParking());
            favori.setFavori(true);
            favori.save();
            btnFavori.setText("- Retirer de mes favoris");
        }
    }

    /**
     * Verifie si le parking est en favori
     * @return
     */
    public boolean isFavori(){
        Favori favori = DaoFavori.getFavoriById(parking.getIdParking());
        if(favori == null){
            return false;
        } else {
            return favori.isFavori();
        }
    }

    /**
     * Redirection vers la carte avec ce parking en paramètre
     */
    @OnClick(R.id.MapsViewButton)
    public void redirectionVersMap() {
        Intent intent = new Intent(getApplicationContext(), ActivityMaps.class);
        intent.putExtra(MainActivity.FILTRE_PARKING, parking.getNomParking());
        startActivity(intent);

    }
}
