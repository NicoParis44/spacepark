package org.miage.spacepark.vue;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import org.miage.spacepark.R;
import org.miage.spacepark.model.Parking;
import org.miage.spacepark.model.dao.DaoParking;
import org.miage.spacepark.ui.ParkingAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by utilisateur on 14/04/2018.
 */

public class ActivityRechercheParking extends AppCompatActivity {

    @BindView(R.id.nomParking)
    EditText nomParking;

    @BindView(R.id.adresseParking)
    EditText adresse;

    @BindView(R.id.carte)
    CheckBox carte;

    @BindView(R.id.cheque)
    CheckBox cheque;

    @BindView(R.id.espece)
    CheckBox espece;

    @BindView(R.id.seekBarMin)
    SeekBar valeurMin;

    @BindView(R.id.numberPlaceMin)
    TextView afficheValeurMin;

    @BindView(R.id.seekBarMax)
    SeekBar valeurMax;

    @BindView(R.id.numberPlaceMax)
    TextView afficheValeurMax;

    @BindView(R.id.rechercher)
    Button recherche;

    @BindView(R.id.creerRecherche)
    LinearLayout creerRecherche;

    @BindView(R.id.reponseRecherche)
    LinearLayout reponseRecherche;

    @BindView(R.id.parkingListe)
    ListView lParking;

    private List<Parking> listeParking;

    public static final String PARKING_SELECTIONNER = "id_parking";

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recherche_parking);
        ButterKnife.bind(this);

        initialiser();
        setParkingItemClickListener();
        gestionAffichageValeurMin();
        gestionAffichageValeurMax();
    }

    /**
     * Gestion pour afficher la valeur de la progress Bar dans une textView
     */
    private void gestionAffichageValeurMin() {
        valeurMin.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {


            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                afficheValeurMin.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }


        });
    }

    /**
     * Initialise les valeurs des progress bar
     */
    private void initialiser() {
        valeurMin.setProgress(0);
        afficheValeurMin.setText("0");

        valeurMax.setProgress(663);
        afficheValeurMax.setText("663");

        reponseRecherche.setVisibility(View.INVISIBLE);
    }

    /**
     * Lance la recherche selon les paramètres
     */
    @OnClick(R.id.rechercher)
    public void rechercherParking() {
        controleRecherche();
        if(listeParking != null && !listeParking.isEmpty()) {
            ParkingAdapter adapter = new ParkingAdapter(this, listeParking);
            lParking.setAdapter(adapter);

            creerRecherche.setVisibility(View.GONE);
            reponseRecherche.setVisibility(View.VISIBLE);
        } else {
            Toast.makeText(getApplicationContext(), "Aucun parking ne correponds a vos recherche", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Permet de récupéré les parkings selon les paramètres de recherche
     */
    private void controleRecherche() {

        listeParking = DaoParking.getParkingAvecRecherche(nomParking.getText().toString(),
                adresse.getText().toString(),
                afficheValeurMin.getText().toString(),
                afficheValeurMax.getText().toString(),
                carte.isChecked(),
                cheque.isChecked(),
                espece.isChecked());
    }

    /**
     * Gestion pour afficher la valeur de la progress Bar dans une textView
     */
    private void gestionAffichageValeurMax() {
        valeurMax.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {


            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                afficheValeurMax.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }


        });
    }

    /**
     * Méthode pour réagir quand on clique sur un parking
     */
    protected void setParkingItemClickListener() {
        lParking.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(), ActivityDetailParking.class);
                intent.putExtra(PARKING_SELECTIONNER, (listeParking.get(i).getIdParking()));
                startActivity(intent);
            }
        });
    }
}
