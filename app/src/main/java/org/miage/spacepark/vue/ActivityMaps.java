

package org.miage.spacepark.vue;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;
import android.widget.Toast;

import org.miage.spacepark.MainActivity;
import org.miage.spacepark.R;
import org.miage.spacepark.event.EventBusManager;
import org.miage.spacepark.model.Parking;
import org.miage.spacepark.model.dao.DaoParking;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * This shows how to create a simple activity with streetview
 */
public class ActivityMaps extends Menu
        implements OnMapReadyCallback {

    private GoogleMap mMap;
    private List<Parking> listeParking;

    private Context context;
    private Map<String, Parking> mMarkersToParking = new LinkedHashMap<>();

    @BindView(R.id.searchParking)
    TextView recherche;

    SupportMapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);
        mapFragment = new SupportMapFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.map, mapFragment)
                .commit();
        context = this;
        initialiserMenu();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            MapsInitializer.initialize(context.getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mapFragment.getMapAsync(this);
        rechercher();
    }

    @Override
    protected void onPause(){
        // Unregister from Event bus : if event are posted now, the activity will not receive it
        EventBusManager.BUS.unregister(this);


        super.onPause();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        listeParking = DaoParking.allParking();
        if( getIntent() != null && getIntent().getExtras() != null) {
            String filtreParking = getIntent().getExtras().get(MainActivity.FILTRE_PARKING).toString();
            recherche.setText(filtreParking);
        }
        placerParking(listeParking);



        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Parking associatedParking = mMarkersToParking.get(marker.getId());
                if (associatedParking != null) {
                    Intent seePlaceDetailIntent = new Intent(getApplicationContext(), ActivityDetailParking.class);
                    seePlaceDetailIntent.putExtra(MainActivity.PARKING_SELECTIONNER, associatedParking.getIdParking());
                    startActivity(seePlaceDetailIntent);
                }
            }
        });
    }

    /**
     * Permet de placer les parkings sur la map
     * @param listeParking
     */
    private void placerParking(List<Parking> listeParking) {
        LatLngBounds.Builder cameraBounds = LatLngBounds.builder();
        mMarkersToParking.clear();
        mMap.clear();
        for(Parking parking : listeParking) {
            String messageSnippet = creerMessageSnippet(parking);
            LatLng coordonneParking = new LatLng(parking.getLatitude(), parking.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions().position(coordonneParking).title(parking.getNomParking());
            markerOptions.snippet(messageSnippet);

            Marker marker =  mMap.addMarker(markerOptions);
            mMarkersToParking.put(marker.getId(), parking);
            cameraBounds.include(markerOptions.getPosition());
        }
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.10); // offset from edges of the map 10% of screen

        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(cameraBounds.build(), width, height, padding));
    }

    /**
     * Creer le message que l'on met dans la fenetre du curseur
     * @param parking
     * @return
     */
    private String creerMessageSnippet(Parking parking) {
        StringBuilder message = new StringBuilder();
        message.append(parking.getNomAdresse());
        message.append(" ");
        message.append(String.valueOf(parking.getCodePostale()));
        message.append(" ");
        message.append(parking.getCommune());
        message.append("; Places disponibles : ");
        message.append(parking.getNbPlacesDisponible());
        return message.toString();
    }

    /**
     * Méthode permettant de filtrer en temps réel les parkings selon le nom ou l'adresse
     */
    private void rechercher() {

        recherche.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // Nothing to do when texte is about to change
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // While text is changing, hide list and show loader
            }

            @Override
            public void afterTextChanged(Editable editable) {
                // Once text has changed
                // Show a loader
                listeParking = DaoParking.getParkingRechercher(editable.toString());
                if(!listeParking.isEmpty()) {
                    placerParking(listeParking);
                } else {
                    mMarkersToParking.clear();
                    mMap.clear();
                    Toast.makeText(getApplicationContext(), "Aucun parking ne correponds a vos recherche", Toast.LENGTH_SHORT).show();
                }




            }
        });

    }


}