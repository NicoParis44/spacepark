package org.miage.spacepark.vue;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


import org.miage.spacepark.MainActivity;
import org.miage.spacepark.R;
import org.miage.spacepark.model.Parking;
import org.miage.spacepark.model.dao.DaoParking;
import org.miage.spacepark.ui.ParkingAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import android.support.v4.app.Fragment;

/**
 * Created by utilisateur on 21/04/2018.
 */

public class ActivityFavori extends Menu {

    @BindView(R.id.parkingListe)
    ListView listeParking;

    List<Parking> lParking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_favori);
        setContentView(R.layout.activity_favori);
        ButterKnife.bind(this);
        initialiserMenu();
        setParkingItemClickListener();
    }


    @Override
    protected void onResume() {
        super.onResume();
        lParking = DaoParking.rechercherFavori();
        ArrayAdapter adapter = new ParkingAdapter(this,lParking);
        listeParking.setAdapter(adapter);
    }

    /**
     * Méthode pour réagir quand on clique sur un parking
     */
    protected void setParkingItemClickListener() {
        listeParking.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(), ActivityDetailParking.class);
                intent.putExtra(MainActivity.PARKING_SELECTIONNER, (lParking.get(i).getIdParking()));
                startActivity(intent);
            }
        });
    }
}
