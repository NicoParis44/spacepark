package org.miage.spacepark.vue;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import org.miage.spacepark.MainActivity;
import org.miage.spacepark.R;

import butterknife.OnClick;

/**
 * Created by utilisateur on 24/04/2018.
 */

public class Menu extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    protected NavigationView navigationView;

    protected void onCreate(@Nullable Bundle savedInstanceState, int layout) {

        super.onCreate(savedInstanceState);
        setContentView(layout);
        initialiserMenu();
    }

    /**
     * Gestion du menu
     * @param item
     * @return
     */
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.menuMaps) {
            // Handle the camera action
            Intent intent = new Intent(getApplicationContext(), ActivityMaps.class);
            startActivity(intent);
        } else if (id == R.id.menuSearch) {

            Intent intent = new Intent(getApplicationContext(), ActivityRechercheParking.class);
            startActivity(intent);

        } else if (id == R.id.menuFavori) {
            Intent intent = new Intent(getApplicationContext(), ActivityFavori.class);
            startActivity(intent);

        } else if (id == R.id.menuListe) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @OnClick
    public void floatingAction(View view) {
        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    /**
     * Initialise le menu
     */
    protected void initialiserMenu() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }
}
