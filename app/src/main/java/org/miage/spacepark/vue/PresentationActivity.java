package org.miage.spacepark.vue;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;

import org.miage.spacepark.MainActivity;
import org.miage.spacepark.ParkingSearchService;
import org.miage.spacepark.R;
import org.miage.spacepark.event.EventBusManager;
import org.miage.spacepark.model.DisponibiliteParking;
import org.miage.spacepark.model.Favori;
import org.miage.spacepark.model.Parking;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PresentationActivity extends AppCompatActivity {
    @BindView(R.id.pageAll)
    LinearLayout pageAll;
    @BindView(R.id.pageRecherche)
    LinearLayout pageRecherche;
    @BindView(R.id.pageMaps)
    LinearLayout pageMaps;
    @BindView(R.id.pageFavoris)
    LinearLayout pageFavoris;

    @Override
    protected void onResume() {
        super.onResume();
        EventBusManager.BUS.register(this);
        ParkingSearchService.INSTANCE.searchParking();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_presentation);
        ButterKnife.bind(this);
        Configuration.Builder config = new Configuration.Builder(this); config.addModelClasses(Parking.class,
                DisponibiliteParking.class, Favori.class);
        ActiveAndroid.initialize(config.create());
        pageAll.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }});

        pageRecherche.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ActivityRechercheParking.class);
                startActivity(intent);
            }});

        pageMaps.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ActivityMaps.class);
                startActivity(intent);
            }});

        pageFavoris.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ActivityFavori.class);
                startActivity(intent);

            }});


    }

    @Override
    protected void onPause() {
        // Unregister from Event bus : if event are posted now, the activity will not receive it
        EventBusManager.BUS.unregister(this);


        super.onPause();
    }


}
