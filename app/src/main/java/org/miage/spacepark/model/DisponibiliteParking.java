package org.miage.spacepark.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by utilisateur on 09/04/2018.
 */
@Table(name = "DisponibiliteParking")
public class DisponibiliteParking extends Model {

    @Expose
    @SerializedName("Grp_identifiant")
    private String identifiant;

    @Expose
    @SerializedName("Grp_nom")
    @Column(name = "nomParking")
    private String nomParking;

    @Expose
    @SerializedName("Grp_statut")
    @Column(name = "status")
    private String status;

    @Expose
    @SerializedName("Grp_pri_aut")
    private String prioriteModeAutomatique;

    @Expose
    @SerializedName("Grp_disponible")
    @Column(name = "nbPlaceDisponible")
    private String nbPlacesDisponible;

    @Expose
    @SerializedName("Grp_complet")
    @Column(name = "nbPlaceAffichageComplet")
    private String nbParkingComplet;

    @Expose
    @SerializedName("Grp_exploitation")
    @Column(name = "nbPlace")
    private String nbPlaceExistant;

    @Expose
    @SerializedName("Grp_horodatage")
    private String horodatage;

    @Expose
    @SerializedName("IdObj")
    @Column(name = "idParking", index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private String idParkingBDD;


    public String getIdentifiant() {
        return identifiant;
    }

    public String getNomParking() {
        return nomParking;
    }

    public String getStatus() {
        return status;
    }

    public String getPrioriteModeAutomatique() {
        return prioriteModeAutomatique;
    }

    public String getNbPlacesDisponible() {
        return nbPlacesDisponible;
    }

    public String getNbParkingComplet() {
        return nbParkingComplet;
    }

    public String getNbPlaceExistant() {
        return nbPlaceExistant;
    }

    public String getHorodatage() {
        return horodatage;
    }

    public String getIdParkingBDD() {
        return idParkingBDD;
    }
}
