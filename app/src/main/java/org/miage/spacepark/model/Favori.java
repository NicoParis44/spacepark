package org.miage.spacepark.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by utilisateur on 20/04/2018.
 */
@Table(name = "Favori")
public class Favori extends Model {

    @Column(name = "idParking", index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private int idParking;

    @Column(name = "favori")
    private boolean favori = false;

    public int getIdParking() {
        return idParking;
    }

    public void setIdParking(int idParking) {
        this.idParking = idParking;
    }

    public boolean isFavori() {
        return favori;
    }

    public void setFavori(boolean favori) {
        this.favori = favori;
    }
}
