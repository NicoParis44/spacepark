package org.miage.spacepark.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by utilisateur on 09/02/2018.
 */

@Table(name = "Parking")
public class Parking extends Model {

    @Expose
    @SerializedName("SERVICE_VELO")
    @Column(name = "serviceVelo")
    private String serviceVelo;

    @Expose
    @SerializedName("CAPACITE_VOITURE")
    @Column(name = "nbPlaceVoiture")
    private int nbPlaceVoiture;

    @Expose
    @SerializedName("STATIONNEMENT_VELO")
    @Column(name = "emplacementVelo")
    private String emplacementVelo;

    @Expose
    @SerializedName("INFOS_COMPLEMENTAIRES")
    @Column(name = "information")
    private String information;

    @Expose
    @SerializedName("STATIONNEMENT_VELO_SECURISE")
    @Column(name = "emplacementVeloSecuriser")
    private String emplacementVeloSecuriser;

    @Expose
    @SerializedName("geo")
    private NomParking pNomParking;

    @Expose
    @SerializedName("_l")
    private List<Double> geolocalisation;

    @Expose
    @SerializedName("CAPACITE_VEHICULE_ELECTRIQUE")
    @Column(name = "nbPlaceVoitureElectrique")
    private int nbPlaceVoitureElectrique;

    @Expose
    @SerializedName("CODE_POSTAL")
    @Column(name = "codePostale")
    private int codePostale;

    @Expose
    @SerializedName("ACCES_PMR")
    @Column(name = "accesPersonneHandicape")
    private String accesPersonneHandicape;

    @Expose
    @SerializedName("TELEPHONE")
    @Column(name = "numeroTelephone")
    private String numeroTelephone;

    @Expose
    @SerializedName("MOYEN_PAIEMENT")
    private String moyenPaiement;

    @Expose
    @SerializedName("ACCES_TRANSPORTS_COMMUNS")
    @Column(name = "transportCommun")
    private String transportCommun;

    @Expose
    @SerializedName("CAPACITE_MOTO")
    @Column(name = "nbPlaceMoto")
    private int nbPlaceMoto;

    @Expose
    @SerializedName("CONDITIONS_D_ACCES")
    @Column(name = "conditionAcces")
    private String conditionAcces;

    @Expose
    @SerializedName("COMMUNE")
    @Column(name = "commune")
    private String commune;

    @Expose
    @SerializedName("CAPACITE_VELO")
    @Column(name = "nbPlaceVelo")
    private int nbPlaceVelo;

    @Expose
    @SerializedName("PRESENTATION")
    @Column(name = "presentation")
    private String presentation;

    @Expose
    @SerializedName("CAPACITE_PMR")
    @Column(name = "nbPlaceHandicape")
    private int nbPlaceHandicape;

    @Expose
    @SerializedName("LIBCATEGORIE")
    @Column(name = "categorieParking")
    private String categorieParking;

    @Expose
    @SerializedName("EXPLOITANT")
    @Column(name = "exploitantParking")
    private String exploitantParking;

    @Expose
    @SerializedName("SITE_WEB")
    @Column(name = "siteWeb")
    private String siteWeb;

    @Expose
    @SerializedName("LIBTYPE")
    @Column(name = "typeParking")
    private String typeParking;

    @Expose
    @SerializedName("ADRESSE")
    @Column(name = "nomAdresse")
    private String nomAdresse;

    @Expose
    @SerializedName("_IDOBJ")
    @Column(name = "idParking", index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private int idParking;

    @Column(name = "nomParking")
    private String nomParking;

    @Column(name = "longitude")
    private double longitude;

    @Column(name = "latitude")
    private double latitude;

    @Column(name = "nbPlaceDisponible")
    private int nbPlacesDisponible;



    @Column(name="cheque")
    private boolean cheque;

    @Column(name = "espece")
    private boolean espece;

    @Column(name = "carteBancaire")
    private boolean CB;

    public Parking() {
        super();
    }

    public String getServiceVelo() {
        return serviceVelo;
    }

    public void setServiceVelo(String serviceVelo) {
        this.serviceVelo = serviceVelo;
    }

    public int getNbPlaceVoiture() {
        return nbPlaceVoiture;
    }

    public void setNbPlaceVoiture(int nbPlaceVoiture) {
        this.nbPlaceVoiture = nbPlaceVoiture;
    }

    public String getEmplacementVelo() {
        return emplacementVelo;
    }

    public void setEmplacementVelo(String emplacementVelo) {
        this.emplacementVelo = emplacementVelo;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public String getEmplacementVeloSecuriser() {
        return emplacementVeloSecuriser;
    }

    public void setEmplacementVeloSecuriser(String emplacementVeloSecuriser) {
        this.emplacementVeloSecuriser = emplacementVeloSecuriser;
    }

    public NomParking getpNomParking() {
        return pNomParking;
    }

    public void setpNomParking(NomParking pNomParking) {
        this.pNomParking = pNomParking;
    }

    public List<Double> getGeolocalisation() {
        return geolocalisation;
    }

    public void setGeolocalisation(List<Double> geolocalisation) {
        this.geolocalisation = geolocalisation;
    }

    public int getNbPlaceVoitureElectrique() {
        return nbPlaceVoitureElectrique;
    }

    public void setNbPlaceVoitureElectrique(int nbPlaceVoitureElectrique) {
        this.nbPlaceVoitureElectrique = nbPlaceVoitureElectrique;
    }

    public int getCodePostale() {
        return codePostale;
    }

    public void setCodePostale(int codePostale) {
        this.codePostale = codePostale;
    }

    public String getAccesPersonneHandicape() {
        return accesPersonneHandicape;
    }

    public void setAccesPersonneHandicape(String accesPersonneHandicape) {
        this.accesPersonneHandicape = accesPersonneHandicape;
    }

    public String getNumeroTelephone() {
        return numeroTelephone;
    }

    public void setNumeroTelephone(String numeroTelephone) {
        this.numeroTelephone = numeroTelephone;
    }

    public String getMoyenPaiement() {
        return moyenPaiement;
    }

    public void setMoyenPaiement(String moyenPaiement) {
        this.moyenPaiement = moyenPaiement;
    }

    public String getTransportCommun() {
        return transportCommun;
    }

    public void setTransportCommun(String transportCommun) {
        this.transportCommun = transportCommun;
    }

    public int getNbPlaceMoto() {
        return nbPlaceMoto;
    }

    public void setNbPlaceMoto(int nbPlaceMoto) {
        this.nbPlaceMoto = nbPlaceMoto;
    }

    public String getConditionAcces() {
        return conditionAcces;
    }

    public void setConditionAcces(String conditionAcces) {
        this.conditionAcces = conditionAcces;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public int getNbPlaceVelo() {
        return nbPlaceVelo;
    }

    public void setNbPlaceVelo(int nbPlaceVelo) {
        this.nbPlaceVelo = nbPlaceVelo;
    }

    public String getPresentation() {
        return presentation;
    }

    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }

    public int getNbPlaceHandicape() {
        return nbPlaceHandicape;
    }

    public void setNbPlaceHandicape(int nbPlaceHandicape) {
        this.nbPlaceHandicape = nbPlaceHandicape;
    }

    public String getCategorieParking() {
        return categorieParking;
    }

    public void setCategorieParking(String categorieParking) {
        this.categorieParking = categorieParking;
    }

    public String getExploitantParking() {
        return exploitantParking;
    }

    public void setExploitantParking(String exploitantParking) {
        this.exploitantParking = exploitantParking;
    }

    public String getSiteWeb() {
        return siteWeb;
    }

    public void setSiteWeb(String siteWeb) {
        this.siteWeb = siteWeb;
    }

    public String getTypeParking() {
        return typeParking;
    }

    public void setTypeParking(String typeParking) {
        this.typeParking = typeParking;
    }

    public String getNomAdresse() {
        return nomAdresse;
    }

    public void setNomAdresse(String nomAdresse) {
        this.nomAdresse = nomAdresse;
    }

    public int getIdParking() {
        return idParking;
    }

    public void setIdParking(int idParking) {
        this.idParking = idParking;
    }

    public String getNomParking() {
        return nomParking;
    }

    public void setNomParking(String nomParking) {
        this.nomParking = nomParking;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public int getNbPlacesDisponible() {
        return nbPlacesDisponible;
    }

    public void setNbPlacesDisponible(int nbPlacesDisponible) {
        this.nbPlacesDisponible = nbPlacesDisponible;
    }

    public boolean isCheque() {
        return cheque;
    }

    public void setCheque(boolean cheque) {
        this.cheque = cheque;
    }

    public boolean isEspece() {
        return espece;
    }

    public void setEspece(boolean espece) {
        this.espece = espece;
    }

    public boolean isCB() {
        return CB;
    }

    public void setCB(boolean CB) {
        this.CB = CB;
    }
}
