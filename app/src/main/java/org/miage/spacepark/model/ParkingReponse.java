package org.miage.spacepark.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by utilisateur on 19/02/2018.
 */

public class ParkingReponse {

    @Expose
    @SerializedName("version")
    private String version;

    @Expose
    @SerializedName("nb_results")
    private int nbResultat;

    @Expose
    @SerializedName("data")
    private List<Parking> listeParking;

    public String getVersion() {
        return version;
    }

    public int getNbResultat() {
        return nbResultat;
    }

    public List<Parking> getListeParking() {
        return listeParking;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setNbResultat(int nbResultat) {
        this.nbResultat = nbResultat;
    }

    public void setListeParking(List<Parking> listeParking) {
        this.listeParking = listeParking;
    }
}
