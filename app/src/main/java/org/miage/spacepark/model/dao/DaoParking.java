package org.miage.spacepark.model.dao;

import com.activeandroid.query.Select;

import org.apache.commons.lang3.StringUtils;
import org.miage.spacepark.model.Favori;
import org.miage.spacepark.model.Parking;

import java.util.List;

/**
 * Created by utilisateur on 10/04/2018.
 */

public class DaoParking {

    /**
     * Récupére tous les parkings présent dans la base
     * @return
     */
    public static List<Parking> allParking() {
        return new Select().
                from(Parking.class)
                .orderBy("idParking")
                .execute();
    }

    /**
     * Récupére un parking selon son id
     * @param idParking
     * @return
     */
    public static Parking getParkingByIdParking(int idParking) {
        return new Select()
                .from(Parking.class)
                .where("idParking = ?", idParking)
                .executeSingle();
    }

    /**
     * Récupere les parkings selon les paramtrès de recherche
     * @param nom le nom du parking
     * @param adresse l'adresse du parking
     * @param valeurMin le nombre minimum de place
     * @param valeurMax le nombre maximum de place
     * @param carte le paiement par carte est accepté
     * @param cheque le paiement par cheque est accepté
     * @param espece le paiement par espece est accepté
     * @return
     */
    public static List<Parking> getParkingAvecRecherche(
            String nom, String adresse, String valeurMin, String valeurMax,
            boolean carte, boolean cheque, boolean espece) {
        String condition = creationRequeteRecherche(nom, adresse, valeurMin, valeurMax, carte, cheque, espece);

        return new Select()
                .from(Parking.class)
                .where(condition)
                .execute();
    }

    /**
     * Créer la requete de recherche
     * @param nom le nom du parking
     * @param adresse l'adresse du parking
     * @param valeurMin le nombre minimum de place
     * @param valeurMax le nombre maximum de place
     * @param carte le paiement par carte est accepté
     * @param cheque le paiement par cheque est accepté
     * @param espece le paiement par espece est accepté
     * @return
     */
    private static String creationRequeteRecherche(String nom, String adresse, String valeurMin, String valeurMax, boolean carte, boolean cheque, boolean espece) {
        StringBuilder condition = new StringBuilder();
        condition.append("nbPlaceDisponible BETWEEN " + valeurMin);
        condition.append(" AND " + valeurMax);

        if(StringUtils.isNotBlank(nom)) {
            condition.append(" AND lower(nomParking) like '%");
            condition.append(nom + "%'");
        }

        if(StringUtils.isNotBlank(adresse)) {
            condition.append(" AND lower(nomAdresse) like '%");
            condition.append(adresse + "%'");
        }

        if(carte) {
            condition.append(" AND carteBancaire");
        }

        if(cheque) {
            condition.append(" AND cheque");
        }

        if(espece) {
            condition.append(" AND espece");
        }
        return condition.toString();
    }

    /**
     * Récupére les parkings qui sont en favori
     * @return
     */
    public static List<Parking> rechercherFavori() {
        return new Select()
                .from(Parking.class)
                .innerJoin(Favori.class)
                .on("Parking.idParking=Favori.idParking")
                .where("Favori.favori")
                .execute();
    }

    /**
     * Récupére les parkings selon le filtre en temps réel
     * @param s
     * @return
     */
    public static List<Parking> getParkingRechercher(String s) {

        return new Select()
                .from(Parking.class)
                .where("lower(nomParking) LIKE '%" + s + "%' OR lower(nomAdresse) LIKE '%" + s + "%'")
                .execute();
    }
}
