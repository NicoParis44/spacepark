package org.miage.spacepark.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by utilisateur on 09/04/2018.
 */

public class DisponibiliteParkingReponse {

    @Expose
    @SerializedName("opendata")
    private ReponseApi contenuReponse;

    public ReponseApi getContenuReponse() {
        return contenuReponse;
    }

    public class ReponseApi {

        @Expose
        @SerializedName("request")
        private String requete;

        @Expose
        @SerializedName("answer")
        private Reponse reponse;

        public String getRequete() {
            return requete;
        }

        public Reponse getReponse() {
            return reponse;
        }

    }

    public class Reponse {

        @Expose
        @SerializedName("status")
        private Status status;

        @Expose
        @SerializedName("data")
        private Donnees donnees;

        public Status getStatus() {
            return status;
        }

        public Donnees getDonnees() {
            return donnees;
        }

    }

    public class Donnees {

        @Expose
        @SerializedName("Groupes_Parking")
        private GroupesParking groupesParking;

        public GroupesParking getGroupesParking() {
            return groupesParking;
        }

    }

    public class GroupesParking {

        @Expose
        @SerializedName("Groupe_Parking")
        private List<DisponibiliteParking> disponibiliteParkings;

        public List<DisponibiliteParking> getDisponibiliteParkings() {
            return disponibiliteParkings;
        }
    }

    public class Status {

        @Expose
        @SerializedName("@attributes")
        private Attributes attribut;

        public Attributes getAttribut() {
            return attribut;
        }
    }

    public class Attributes {

        @Expose
        @SerializedName("code")
        private String code;

        @Expose
        @SerializedName("message")
        private String message;

        public String getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }
    }
}
