package org.miage.spacepark.model.dao;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;

import org.miage.spacepark.model.DisponibiliteParking;

import java.util.List;

/**
 * Created by utilisateur on 10/04/2018.
 */

public class DaoDisponibilite {

    /**
     * Permet de récupéré un élément selon son id
     * @param idParking
     * @return
     */
    public static DisponibiliteParking getDisponibiliteById(int idParking) {
        return new Select()
                .from(DisponibiliteParking.class)
                .where("idParking = ?", idParking)
                .executeSingle();

    }

    /**
     * Enregistre en base une liste de DisponibiliteParking
     * @param listeParking
     */
    public static void enregistrerDisponilite(List<DisponibiliteParking> listeParking) {
        ActiveAndroid.beginTransaction();
        for (DisponibiliteParking parking : listeParking) {
            parking.save();
        }
        ActiveAndroid.setTransactionSuccessful();
        ActiveAndroid.endTransaction();
    }
}
