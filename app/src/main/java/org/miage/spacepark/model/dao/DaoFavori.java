package org.miage.spacepark.model.dao;

import com.activeandroid.query.Select;

import org.miage.spacepark.model.Favori;

/**
 * Created by utilisateur on 20/04/2018.
 */

public class DaoFavori {

    /**
     * Récupére un favori selon son id
     * @param idParking
     * @return
     */
    public static Favori getFavoriById(int idParking) {
        return new Select()
                .from(Favori.class)
                .where("idParking = ?", idParking)
                .executeSingle();
    }
}
