package org.miage.spacepark.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by utilisateur on 19/02/2018.
 */

public class NomParking {

    @Expose
    @SerializedName("name")
    private String nom;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
