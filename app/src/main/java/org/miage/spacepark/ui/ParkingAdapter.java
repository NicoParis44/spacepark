package org.miage.spacepark.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import org.miage.spacepark.R;
import org.miage.spacepark.model.Favori;
import org.miage.spacepark.model.Parking;
import org.miage.spacepark.model.dao.DaoFavori;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by utilisateur on 17/02/2018.
 */

public class ParkingAdapter extends ArrayAdapter<Parking>{

    @BindView(R.id.nom)
    TextView nom;

    @BindView(R.id.adresse)
    TextView adresse;

    @BindView(R.id.nbPlace)
    TextView nbPlaces;

    @BindView(R.id.cb)
    ImageView cb;

    @BindView(R.id.cheque)
    ImageView cheque;

    @BindView(R.id.espece)
    ImageView espece;

    @BindView(R.id.favori)
    ImageView favori;

    Context contextActivity;

    public ParkingAdapter(@NonNull Context context, List<Parking> parkings) {
        super(context, -1, parkings);
        contextActivity = context;
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent){
        View actualView = convertView;

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            actualView = inflater.inflate(R.layout.parking_adapter, parent, false);
        }

        ButterKnife.bind(this, actualView);
        nom.setText(getItem(position).getNomParking() == null || getItem(position).getNomParking().equals("")? "NomDuParking" : getItem(position).getNomParking());

        adresse.setText(getItem(position).getNomAdresse());
        nbPlaces.setText("Nombre de place : " + String.valueOf(getItem(position).getNbPlacesDisponible())
                + "/" + String.valueOf(getItem(position).getNbPlaceVoiture()));

        cb.setVisibility(View.VISIBLE);
        if(!getItem(position).isCB()){//S'il n'y a pas de paiement CB
            cb.setVisibility(View.INVISIBLE);//on masque l'icone
        }
        espece.setVisibility(View.VISIBLE);
        if(!getItem(position).isEspece()){//S'il n'y a pas de paiement Espèce
            espece.setVisibility(View.INVISIBLE);//on masque l'icone
        }
        cheque.setVisibility(View.VISIBLE);
        if(!getItem(position).isCheque()){//S'il n'y a pas de paiement cheque
            cheque.setVisibility(View.INVISIBLE);//on masque l'icone
        }
        Favori favoriParking  = DaoFavori.getFavoriById(getItem(position).getIdParking());
        favori.setVisibility(View.VISIBLE);
        if(favoriParking == null || !favoriParking.isFavori()) {
            favori.setVisibility(View.INVISIBLE);


        }


        return actualView;
    }
}
