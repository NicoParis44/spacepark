package org.miage.spacepark;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;
import com.squareup.otto.Subscribe;

import org.miage.spacepark.event.EventBusManager;
import org.miage.spacepark.event.SearchResultEvent;
import org.miage.spacepark.model.DisponibiliteParking;
import org.miage.spacepark.model.Parking;
import org.miage.spacepark.model.dao.DaoParking;
import org.miage.spacepark.ui.ParkingAdapter;
import org.miage.spacepark.vue.ActivityDetailParking;
import org.miage.spacepark.vue.ActivityMaps;

import java.util.List;

import butterknife.ButterKnife;

/**
 * Classe pour afficher les parkings sous la forme de liste
 */
public class MainActivity extends org.miage.spacepark.vue.Menu {

    public static final String PARKING_SELECTIONNER = "id_parking";
    public static final String FILTRE_PARKING = "parking_search";


    ListView listeParking;
    TextView recherche;
    ArrayAdapter adapter;
    List<Parking> lParking;
    Context context;
    ImageView redirectionMaps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_main);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Configuration.Builder config = new Configuration.Builder(this); config.addModelClasses(Parking.class, DisponibiliteParking.class); ActiveAndroid.initialize(config.create());
        context = this;

        recherche = (TextView) findViewById(R.id.searchParking); 
        listeParking = (ListView) findViewById(R.id.parkingListe);
        redirectionMaps = (ImageView) findViewById(R.id.MapsViewButton);

        initialiserMenu();
        setParkingItemClickListener();


    }


    @Override
    protected void onResume() {
        // Do NOT forget to call super.onResume()
        super.onResume();
        rechercher();

        EventBusManager.BUS.register(this);
        ParkingSearchService.INSTANCE.searchParking();
        redirectionMaps.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ActivityMaps.class);
                intent.putExtra(FILTRE_PARKING, recherche.getText());
                startActivity(intent);
            }});
    }

    /**
     * Méthode permettant de filtrer en temps réel les parkings selon le nom ou l'adresse
     */
    private void rechercher() {

        recherche.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // Nothing to do when texte is about to change
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // While text is changing, hide list and show loader
            }

            @Override
            public void afterTextChanged(Editable editable) {
                // Once text has changed
                // Show a loader
                lParking = DaoParking.getParkingRechercher(editable.toString());
                if(lParking.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Aucun parking ne correponds a vos recherche", Toast.LENGTH_SHORT).show();

                }
                adapter = new ParkingAdapter(context, lParking);
                listeParking.setAdapter(adapter);

            }
        });
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    protected void onPause() {
        // Unregister from Event bus : if event are posted now, the activity will not receive it
        EventBusManager.BUS.unregister(this);
        

        super.onPause();
    }

    @Subscribe
    public void searchResult(final SearchResultEvent event) {
        // Here someone has posted a SearchResultEvent
        // Update adapter's model
        lParking = event.getParking().getListeParking();
        adapter = new ParkingAdapter(this, lParking);
        listeParking.setAdapter(adapter);
    }

    /**
     * Méthode pour réagir quand on clique sur un parking
     */
    protected void setParkingItemClickListener() {
        listeParking.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(), ActivityDetailParking.class);
                intent.putExtra(PARKING_SELECTIONNER, (lParking.get(i).getIdParking()));
                startActivity(intent);
            }
        });
    }
}
